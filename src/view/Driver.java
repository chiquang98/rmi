/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.ObjectClient;
import control.StringClient;
import model.Student;
import registerPort.RegisterPortRMI;

/**
 *
 * @author chiquang
 */
public class Driver {

    public static void main(String[] args) {
        int stringServerPort = 1234;
        int numericServerPort = 1235;
        int objectServerPort = 1236;
        new RegisterPortRMI().register(stringServerPort, numericServerPort, objectServerPort);
        String ipServer = "localhost";//kiem tra se la IP may thay
        ObjectClient objectClient = new ObjectClient(ipServer, objectServerPort);
        Student st = new Student("B16DCC", "Ten cua cac ban", "IP ", 4);
        objectClient.sendToServer(st);
        StringClient stringClient = new StringClient(ipServer, stringServerPort);
        stringClient.sendToServer(st);
    }
}
