/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registerPort;

import control.IRMIServer;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ServerConfiguration;
import model.Student;

/**
 *
 * @author chiquang
 */
public class RegisterPortRMI {

   public void register(int stringServer,int numericServer, int objectServer  ) {
        String name = "Server";
        String port = "1099";
        String host = "localhost";// luc kiem tra se la dia chi IP cua may thay
        String URL = "rmi://" + host + ":" + port + "/" + name;
        int code = 1;//ma de
        int stringServerPort = stringServer;
        int numericServerPort = numericServer;
        int objectServerPort = objectServer;
        try {
            IRMIServer myServer = (IRMIServer) Naming.lookup(URL);
            Student std = new Student("B16DCCN", "QQQQ", "192.168.1.1", 4);
            ServerConfiguration config = new ServerConfiguration();
            config.setCode(code);
            config.setObjectServerPort(objectServerPort);
            //For objectServerPort
            ServerConfiguration configuration = myServer.getObjectServerDes(std, config);
            //For StringServer
            config.setStringServerPort(stringServerPort);
            configuration = myServer.getStringServerDes(std, config);
            //For NumericServer
            config.setNumericServerPort(numericServerPort);
            configuration = myServer.getStringServerDes(std, config);

        } catch (NotBoundException ex) {
            Logger.getLogger(RegisterPortRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(RegisterPortRMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(RegisterPortRMI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
