/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import model.Answer;
import model.Student;

/**
 *
 * @author chiquang
 */
public class ObjectClient {

    private Socket socket;

    public ObjectClient(String IP, int port) {
        try {
            socket = new Socket(IP, port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Answer sendToServer(Student student) {
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(student);
            Answer answer = (Answer) ois.readObject();
            return answer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
