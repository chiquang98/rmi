/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import model.*;

/**
 *
 * @author chiquang
 */
public class StringClient {

    Socket socket;

    public StringClient(String IP, int port) {
        try {
            socket = new Socket(IP, port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Answer sendToServer(Student student) {
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            int code = 1;//ma de
            dos.writeUTF(student.getMsv());
            dos.writeUTF(student.getHovaten());
            dos.writeInt(student.getGroup());
            dos.writeInt(code);
            String str1 = dis.readUTF();
            // vi ma de code = 1 nen chi lam phan co code = 1 k can lam phan code = 0
            int[] mark = new int[256];
            for (int i = 0; i < 256; i++) {
                mark[i] = 0;
            }
            for (int i = 0; i < str1.length(); i++) {
                char c = str1.charAt(i);
                mark[c]++;
            }
            //sort
            for (int i = 0; i < 256; i++) {
                for (int j = i + 1; j < 256; j++) {
                    if (mark[i] < mark[j]) {
                        int tmp = mark[i];
                        mark[i] = mark[j];
                        mark[j] = tmp;
                    }
                }
            }
//            char c = (char)mark[1];//cai nhieu thu 2
            dos.writeChar(mark[1]);
            Object str2 = ois.readUTF();
            Object a = ois.readInt();
            Object b = ois.readInt();
            String partten = "";
            String cvStr2 = str2.toString();
            int CVa = Integer.parseInt(a.toString());
            int CVb = Integer.parseInt(b.toString());
            for (int i = 0; i < cvStr2.length(); i++) {
                if (i > CVa && i < CVb) {
                    partten += i;
                }
            }
            dos.writeUTF(partten.trim());
            str1 = dis.readUTF();
            int n = dis.readInt();
            dos.writeUTF(encrypt(str1, n).toString());
            Answer answer = (Answer) ois.readObject();
            
            return answer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static StringBuffer encrypt(String text, int s) {
        StringBuffer result = new StringBuffer();

        for (int i = 0; i < text.length(); i++) {
            if (Character.isUpperCase(text.charAt(i))) {
                char ch = (char) (((int) text.charAt(i)
                        + s - 65) % 26 + 65);
                result.append(ch);
            } else {
                char ch = (char) (((int) text.charAt(i)
                        + s - 97) % 26 + 97);
                result.append(ch);
            }
        }
        return result;
    }

}
