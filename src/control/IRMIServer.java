/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.rmi.Remote;
import model.ServerConfiguration;
import model.Student;

/**
 *
 * @author chiquang
 */
public interface IRMIServer extends Remote{
    ServerConfiguration getObjectServerDes(Student student,ServerConfiguration config);
    ServerConfiguration getStringServerDes(Student student,ServerConfiguration config);
    ServerConfiguration getNumericServerDes(Student student,ServerConfiguration config);
}
