/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author chiquang
 */
public class ServerConfiguration implements Serializable {

    final long serialVersionUID = 3L;
    int stringServerPort;
    int numericServerPort;
    int objectServerPort;
    int code;





    public int getStringServerPort() {
        return stringServerPort;
    }

    public void setStringServerPort(int stringServerPort) {
        this.stringServerPort = stringServerPort;
    }

    public int getNumericServerPort() {
        return numericServerPort;
    }

    public void setNumericServerPort(int numericServerPort) {
        this.numericServerPort = numericServerPort;
    }

    public int getObjectServerPort() {
        return objectServerPort;
    }

    public void setObjectServerPort(int objectServerPort) {
        this.objectServerPort = objectServerPort;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
