/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author chiquang
 */
public class Answer implements Serializable{
    final long serialVersionUID = 2L;
    Student student;
    Object[] answers;
    boolean[] isRights;
    boolean alreadyRegistration;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Object[] getAnswers() {
        return answers;
    }

    public void setAnswers(Object[] answers) {
        this.answers = answers;
    }

    public boolean[] getIsRights() {
        return isRights;
    }

    public void setIsRights(boolean[] isRights) {
        this.isRights = isRights;
    }

    public boolean isAlreadyRegistration() {
        return alreadyRegistration;
    }

    public void setAlreadyRegistration(boolean alreadyRegistration) {
        this.alreadyRegistration = alreadyRegistration;
    }
    
}
