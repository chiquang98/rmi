/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author chiquang
 */
public class Student implements Serializable{
    final long serialVersionUID = 1L;
    String msv;
    String hovaten;
    String IP;
    int group;

    public Student(String msv, String hovaten, String IP, int group) {
        this.msv = msv;
        this.hovaten = hovaten;
        this.IP = IP;
        this.group = group;
    }
    
    public String getMsv() {
        return msv;
    }

    public void setMsv(String msv) {
        this.msv = msv;
    }

    public String getHovaten() {
        return hovaten;
    }

    public void setHovaten(String hovaten) {
        this.hovaten = hovaten;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }
    
}
